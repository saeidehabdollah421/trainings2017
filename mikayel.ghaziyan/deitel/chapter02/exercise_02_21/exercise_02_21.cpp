/// Mikayel Ghaziyan
/// 12/10/2017
/// Exercise 2.21
  
#include <iostream>

int  ///Beginning of the main function
main()
{
    /// Printing out the geometric shapes
    std::cout << "*********        ***           *             *     \n"
	      << "*       *       *   *         ***           * *    \n"
              << "*       *      *     *       *****         *   *   \n"
	      << "*       *      *     *         *          *     *  \n"
	      << "*       *      *     *         *         *       * \n"
	      << "*       *      *     *         *          *     *  \n"
	      << "*       *      *     *         *           *   *   \n"
	      << "*       *       *   *          *            * *    \n"
	      << "*********        ***           *             *     \n";

     return 0; /// Successfully ends the program
}  /// End of the main function


/// End of the file

