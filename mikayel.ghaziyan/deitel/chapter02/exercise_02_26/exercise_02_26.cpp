/// Mikayel Ghaziyan
/// 16/10/2017
/// Exercise 2.26

#include <iostream>

/// Beginning of the main
int
main()
{
    /// 1st variant with eight statements
    std::cout << "* * * * * * * *" << std::endl;   
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *\n" << std::endl;

    /// 2nd variant with one statement
    std::cout << "* * * * * * * * \n"
                 " * * * * * * * * \n"
                 "* * * * * * * * \n"
                 " * * * * * * * * \n"
                 "* * * * * * * * \n"
		 " * * * * * * * * \n"
		 "* * * * * * * * \n"
                 " * * * * * * * * " << std::endl;

    return 0;
} /// End of the main

/// End of the file
