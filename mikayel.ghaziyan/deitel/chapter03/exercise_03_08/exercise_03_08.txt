Mikayel Ghaziyan
06/11/2017
Exercise 3.7


A header file typically the a class definition, where functions and data members are declared. At the same time, the source-code file is used to provide the definitions of the member functions declared in the class’s header file.

Tha main reason of creating header file and the source-code file is to separate the user interface from the implementation. if the header defines what the class is implementing, the source-code defines how it is being imeplemented.

