#include "Employee.hpp"
#include <iostream>
#include <string>

int 
main()
{        
    Employee worker1("John", "Lennon", 200);
    Employee worker2("Paul", "Mccartney", 400);

    int year = 12;
    std::cout << "First name - " << worker1.getFirstName() << "\n";
    std::cout << "Last name - " << worker1.getLastName() << "\n";
    std::cout << "Yearly salary - " << worker1.getSalary() * year << "\n";

    int salary1 =  worker1.getSalary();
    int raised1 = salary1 + (salary1 * 10) / 100; 
    std::cout << "Raised salary - " << raised1 * year << std::endl;  

    std::cout << "First name - " << worker2.getFirstName() << "\n";
    std::cout << "Last name - " << worker2.getLastName() << "\n";
    std::cout << "Yearly salary - " << worker2.getSalary() * year << "\n";

    int salary2 = worker2.getSalary();
    int raised2 = salary2 + (salary2 * 10)/100;
    std::cout << "Raised salary - " << raised2 * year << std::endl;

    return 0;
}

