#include <string>

class Invoice
{
public:
    Invoice(std::string itemNumber, std::string description, int quantity, int price);
    void setItemNumber(std::string itemNumber);
    void setDescription (std::string description);
    void setQuantity (int quantity);
    void setPrice(int price);
    std::string getDescription();
    std::string getItemNumber();
    int getQuantity();
    int getPrice();
    int getInvoiceAmount();
private: 
    std::string partDescription_;
    std::string partNumber_;
    int partQuantity_;
    int partPrice_;
};

