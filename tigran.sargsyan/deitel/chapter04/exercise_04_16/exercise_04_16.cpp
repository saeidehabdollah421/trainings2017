#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        int hours;
        std::cout << "Enter hours worked (-1 to quit): ";
        std::cin >> hours;
        if (-1 == hours) {
            return 0;
        }
        if (hours < 0) {
            std::cout << "Error 1. Hours should be positive." << std::endl;
            return 1;
        }

        double hourlyRate;
        std::cout << "Enter hourly rate of the employee ($00.00): ";
        std::cin >> hourlyRate;
        if (hourlyRate < 0) {
            std::cout << "Error 2. Hourly rate should be positive." << std::endl;
            return 2;
        }

        double salary = hours * hourlyRate;
        if (hours > 40) {
            salary = 1.5 * salary - 20 * hourlyRate; 
        }
        std::cout << "Salary is: $" << std::setprecision(2) << std::fixed << salary << "\n" << std::endl;
    }

    return 0;
}

