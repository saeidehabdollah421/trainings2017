#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5;
    
    std::cout << "Enter 5 integers: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    int  small = number1;
    if (number2 < small) {
        number2 = small;
    }
    if (number3 < small) {
        number3 = small;
    }
    if (number4 < small) {
        number4 = small;
    }
    if (number5 < small) {
        number5 = small;
    }
    
    std::cout << "Smallest number: " << small << std::endl;

    int large = number5;
    if (number4 > large) {
        number4 = large;
    }
    if (number3 > large) {
        number3 = large;
    }
    if (number2 > large) {
        number2 = large;
    }
    if (number1 > large) {
        number1 = large;
    }

    std::cout << "Largest number: " << large << std::endl;
    return 0;
}

