#!/usr/bin/python3

import sys

Zero = ["  ***  ",
        " *   * ",
        "*     *",
        "*     *",
        "*     *",
        " *   * ",
        "  ***  "]

One = [" *    ", 
       "**    ", 
       " *    ", 
       " *    ", 
       " *    ", 
       " *    ", 
       "***   "]

Two = [" ***  ", 
       "* *   ", 
       "* *   ", 
       " *    ", 
       "*     ", 
       "*     ", 
       "***** "]

Three = [" ***  ",
         "*   * ",
         "    * ",
         "  **  ",
         "    * ",
         "*   * ",
         " ***  "]

Four = ["   *  ",
        "  **  ",
        " * *  ",
        "*  *  ",
        "******",
        "   *  ",
        "   *  "]

Five = [" **** ",
        " *    ",
        " *    ",
        "  **  ",
        "    * ",
        "*   * ", 
        " ***  "]

Six = [" ***  ",
       "*     ",
       "*     ",
       "****  ",
       "*   * ",
       "*   * ",
       " ***  "]

Seven = ["*****",
         "    *",
         "   * ",
         "  *  ",
         " *   ",
         "*    ",
         "*    "]

Eight = [" *** ",
         "*   *",
         "*   *",
         " *** ",
         "*   *",
         "*   *",
         " *** "]

Nine = [" ****",
        "*   *",
        "*   *",
        " ****",
        "    *",
        "    *",
        "    *"]

Digits = [Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine]

def numberSeperate(digits):
    for number in digits:
        printNumber(number)

def numberSymbol(symbolPart, numberSym):
    line = ""
    for sym in symbolPart:
        line += numberSym if(sym == "*") else " "
    return line

    
def printNumber(digit):
    for symbols in Digits[int(digit)]:
        print(numberSymbol(symbols, digit))
try:
    digits = sys.argv[1]
    numberSeperate(digits)
except IndexError:
    print("usage: bigdigits.py <number>")
except ValueError as err:
    print(err, "in", digits)
