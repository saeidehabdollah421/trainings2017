#include <iostream>
#include <iomanip>

int
main()
{
    double sales = 0;
    
    while (sales != 1) {
        std::cout << "\nEnter sales in dollars (-1 to end): ";
        std::cin  >> sales;
        if (-1 == sales) {
            return 0;
        } 
        if (sales < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            return 1;
        }

        double salary = sales * 0.09 + 200;
        std::cout << "Salary is: "
                  << std::setprecision(2)
                  << std::fixed
                  << salary << std::endl;
    }
    return 0;
}
