#include <iostream>

int
main()
{
    int amountOfNumbers = 0; 
    double sum = 0.0;    

    while (true) {
        int number;
        std::cout << "Enter the number (enter 9999 to exit): ";
        std::cin  >> number;
        if (9999 == number) {
            break;
        }
        sum += number;
        ++amountOfNumbers;
    }
    if (amountOfNumbers != 0) {
        std::cout << "Average -> " << sum / amountOfNumbers << std::endl;
    } 
    return 0;
}

