object: - Types of different watches(example: mechanical watchs, quartz watchs, digital lcd screen watchs etc...).

class: - Watch.

atributes: - The watchs colour, watchs design material(stainless steel, or plastic), clock face design and colour etc...

behaviors: - For example: when a mechanical or analog watch works it ticks, when a digital or lcd screen watch works it blinks, or make a sound etc...

inheritance: - From the clock inherit the devices, like the analog(or digital) watch or timer, stopwatch etc....

abstraction: - We have the class watch. When we build the objects with this class we add some abstractions like "watch with a timer and alarm", 
"watch with alarm and calendar", "watch with GPS navigator" and something like that.

modelling: - For example: the watch body design(stainless steel or plastic, or golden watches, or watches with brilliant cover.), the watch have such a thingslike stopwatch, timer or touch display, or lcd screen and anything like that(It is called functional design).... 

messages: - For example: some watches have sound functions(tower watches have a bell).The message is!!!... (1)when the clock hits 6 oclock or 12, it starts 
sounding or the bell is hitting, (2)In some watches when the alarm reaches its time it starts sounding, (3)Again in some watches when the timer or stopwatch 
reaches its time it starts sounding.

encapsulation: For example: Man use the watch setting buttons to set time or alarm, man use watch alarm to getup every morning at 7:00 oclock, runner use the watch timer to see how many labs he run in 50 seconds, and all the things like that.....

interface: Under interface we can understand the contact of man and watch. It can be a watch clock face or lcd screen, or touch screen, or digital watch 
setting buttons, or mechanical watch setting adjustor etc.....

information hidding: The mechanical watch working mechanism, or digital watch working digital scheme.

data members: Hours, Seconds, Minutes(If the watch has other functions, other parameters are also present.)

data functions: functions of the clock which using the parameters above can work and display hours, or functions that adjust the clock when we press the clocksetting buttons and other things like that.
