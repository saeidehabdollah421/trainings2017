#include<iostream>

int
main()
{
    int side;
    std::cin >> side;

    if (side > 20) {
        std::cout << "Error 1: The value of side must be between 1 and 20 ";
        return 1;
    }
    if (side < 1) {
        std::cout << "Error 1: The value of side must be between 1 and 20 ";
        return 1;
    }
    int i = 1;
    while (i <= side) {
        int row = 1;
        while (row <= side) {
            if (1 == i) {
                std::cout << "*";
            } else if (i == side) {
                std::cout << "*";
            } else if (row == side) {  
                std::cout << "*";
            } else if (1 == row) {
                std::cout << "*";
            } else if (row == side) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++row;
        }
        std::cout << std::endl;
        ++i;
    }
    return 0;
}

