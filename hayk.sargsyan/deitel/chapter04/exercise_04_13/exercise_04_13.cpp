#include <iostream>

int
main() 
{
    float totalDistance = 0;
    float totalFuel = 0;

    while (true) {
        float distance;
        float fuel;

        std::cout << "Enter passed distance (-1 to exit): ";
        std::cin  >> distance;

        if (-1 == distance) {
            break;
        }

        std::cout << "Enter consumption of fuel: ";
        std::cin  >> fuel;

        std::cout << "Miles/gallon for this fueling: " << distance / fuel << std::endl;

        totalDistance += distance;
        totalFuel += fuel;

        std::cout << "Total miles/gallon: " << totalDistance / totalFuel << std::endl << std::endl;
    }

    return 0;
}
