#include "Date.hpp"
#include <iostream>

int 
main()
{
    int userDay;
    int userMonth;
    int userYear;

    std::cout << "Insert month day: ";
    std::cin >> userDay; 
    std::cout << "Insert month : ";
    std::cin >> userMonth;
    std::cout << "Insert year: ";
    std::cin >> userYear;

    Date userDate(userDay, userMonth, userYear);
    userDate.displayDate();

    return 0;
}  
