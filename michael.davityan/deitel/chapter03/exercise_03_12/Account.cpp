#include "Account.hpp"
#include <iostream>

Account::Account(int initialBalance)
{
    if (initialBalance < 0) {
        setBalance(0);
        std::cout << "Info 1: initial balance was invalid." << std::endl;
        return;
    }
    setBalance(initialBalance);
}

void
Account::setBalance(int initialBalance)
{
    balance_ = initialBalance;
}

int 
Account::getBalance()
{
    return balance_;
}

int
Account::credit(int creditValue)
{
    balance_ += creditValue;
    return balance_;
}

int 
Account::debit(int debitValue)
{
    if (debitValue >= balance_) {
        std::cout << "Debit amount exceeded account balance.\n";
        return 0;
    }

    balance_ -= debitValue;
    return balance_;
}

