#include <iostream>

int 
main()
{
    for (int count = 1; count <= 10; ++count) { // loop 10 times
        if (5 == count) { // if count is 5,
            ++count;
        }
        // skip remaining code in loop
        std::cout << count << " ";
    } // end for
    std::cout << "\nUsed continue to skip printing 5" << std::endl;

    return 0; // indicate successful termination
} // end main

