Question:
Assume i = 1 , j = 2 , k = 3 and m = 2 . What does each of the following statements print? Are the parentheses 
necessary in each case?

a. cout << ( i == 1 ) << endl;
b. cout << ( j == 3 ) << endl;
c. cout << ( i >= 1 && j < 4 ) << endl;
d. cout << ( m <= 99 && k < m ) << endl;
e. cout << ( j >= i || k == m ) << endl;
f. cout << ( k + m < j || 3 - j >= k ) << endl;
g. cout << ( !m ) << endl;
h. cout << ( !( j - m ) ) << endl;
i. cout << ( !( k > m ) ) << endl;

Answer: 

a. 1 // true.
b. 0 // false.
c. 1 // true.
d. 0 // false.
e. 1 // true.
f. 0 // false.
g. 0 // false.
h. 1 // true.
i. r variable was not declared.

for g. point parentheses not needed. 
for h. and i. only first parenthesis not needed.
for all other points parenthesis are needed, because without it we have two or much more operands in output operator 
together. Parentesis has the higher priority in all operators, and with parentesis its like we have all in one(its 
mean like one operand......) 
