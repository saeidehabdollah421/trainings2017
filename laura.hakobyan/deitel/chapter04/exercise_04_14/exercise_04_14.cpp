#include <iostream>
#include <iomanip>

int
main()
{
    int accountNumber = 0;
    while (accountNumber != -1) {
        std::cout << "Enter account number (-1 to quit): ";
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (accountNumber < 0) {
            std::cout << "Error 1. Account number cannot be negative." << std::endl;
            return 1;
        }
        double beginingBalance;
        std::cout << "Enter beginning balance: ";
        std::cin >> beginingBalance >> std::setprecision(2) >> std::fixed;
        if (beginingBalance < 0) {
            std::cerr << "Error 2: Balance cannot be negative." << std::endl;
            return 2;
        }

        double totalCharge;
        std::cout << "Enter total charges: ";
        std::cin >> totalCharge >> std::setprecision(2) >> std::fixed;
        if (totalCharge < 0) {
            std::cerr << "Error 3: Total charges cannot be negative." << std::endl;
            return 3;
        }

        double totalCredit;
        std::cout << "Enter total credits: ";
        std::cin >> totalCredit >> std::setprecision(2) >> std::fixed;
        if (totalCredit < 0) {
            std::cerr << "Error 4: Total credit cannot be negative." << std::endl;
            return 4;
        }

        double creditLimit;
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit >> std::setprecision(2) >> std::fixed;
        if (creditLimit < 0) {
            std::cerr << "Error 5: Credit limit cannot be negative." << std::endl;
            return 5;
        }

        double newBalance = beginingBalance + totalCharge - totalCredit;
        std::cout << "New balance is " << newBalance << std::setprecision(2) << std::fixed << std::endl;
        if (newBalance > creditLimit) {
            std::cout << "Account: " << accountNumber << "\n";
            std::cout << "Credit limit: " << creditLimit << "\n" << "Balance: " << newBalance << std::setprecision(2) << std::fixed << std::endl;
            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    }
    return 0;
}
