a. if (age >= 65) ///without ";"
       cout << "Age is greater than or equal to 65" << endl;
   else
       cout << "Age is less than 65" << endl; /// " is in wrong place

b. if (age >= 65)
       cout << "Age is greater than or equal to 65" << endl;
   else  /// without ";"
       cout << "Age is less than 65" << endl; /// " is in wrong place

c. int x = 1, total = 0;
   while (x <= 10)
   {
       total += x;
       x++;
   }

d. while (x <= 100)
   {
       total += x;
       x++;
   }

e. while (y > 0)
   {
       cout << y << endl;
       y--; /// with y++ it will be logic error called an infinite loop
   }
