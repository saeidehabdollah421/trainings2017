#include "Employee.hpp"
#include <iostream>

int
main()
{
    Employee employee1("Ani", "Tovmasyan", 80000);
    Employee employee2("Armen", "Hovakimyan", 120000);
    
    std::cout << employee1.getFirstName() << " " << employee1.getLastName() << "'s yearly salary is " << employee1.getMonthlySalary() * 12 << "AMD" << std::endl;
    int  monthlySalary1 = employee1.getMonthlySalary() * 12 * 1.1;
    employee1.setMonthlySalary(monthlySalary1);
    std::cout << "New yearly salary is " << employee1.getMonthlySalary() << " AMD" << std::endl;

    std::cout << employee2.getFirstName() << " " << employee2.getLastName() << "'s yearly salary is " << employee2.getMonthlySalary() * 12 << " AMD" << std::endl;
    int monthlySalary2 = employee2.getMonthlySalary() * 12 * 1.1;
    employee2.setMonthlySalary(monthlySalary2);
    std::cout << "New yearly salary is " << employee2.getMonthlySalary() << " AMD" << std::endl;
    return 0;
} 

