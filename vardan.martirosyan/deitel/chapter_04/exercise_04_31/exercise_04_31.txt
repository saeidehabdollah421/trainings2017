When (x + y) equal to z and we write in cout z++, this
works because first, it adds 1 to z and then stores
this sum on z. And if we write ++(x + y) the machine
will add 1 and it can not store this sum..

int x = 8, y = 3;
int z = x + y;
std::cout << ++z << std::endl;
 
