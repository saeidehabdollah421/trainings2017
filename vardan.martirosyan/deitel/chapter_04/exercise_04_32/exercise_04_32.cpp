#include <iostream>

int
main()
{
    double firstSide, secondSide, thirdSide;

    std::cout << "Enter first  side: ";
    std::cin  >>  firstSide;
    if (firstSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1;
    }
    std::cout << "Enter second side: ";
    std::cin  >>  secondSide;
    if (secondSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1;
    }
    std::cout << "Enter third  side: ";
    std::cin  >>  thirdSide;
    if (thirdSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1;
    }
    

    if ((firstSide + secondSide) > thirdSide) {
        if ((firstSide + thirdSide) > secondSide) {     
            if ((thirdSide + secondSide) > firstSide) {
                std::cout << "This is a right triangle." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "This is a false triangle." << std::endl;
    return 0;
}
