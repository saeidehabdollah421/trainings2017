#include <iostream>


int
main()
{
    int numberOne, numberTwo, numberThree;

    std::cout << "Enter first number: ";
    std::cin  >> numberOne;
    std::cout << "Enter second number: ";
    std::cin  >> numberTwo;
    std::cout << "Enter Thirth number: ";
    std::cin  >> numberThree;

    std::cout << "Sum = " << (numberOne + numberTwo + numberThree) << std::endl;
    std::cout << "Average value = " <<((numberOne + numberTwo + numberThree) / 3) << std::endl;
    std::cout << "Composition = " << (numberOne * numberTwo * numberThree) << std::endl;


    int max = numberOne;
    if (max < numberTwo) {
        max = numberTwo;
    }
    if (max < numberThree) {
        max = numberThree;
    }
    std::cout << "Max = " << max << std::endl;
    int min = numberOne;
    if (min > numberTwo) {
        min = numberTwo;
    }
    if (min > numberThree) {
        min = numberThree;
    }
    std::cout << "Min = " << min << std::endl;

    return 0;
}

